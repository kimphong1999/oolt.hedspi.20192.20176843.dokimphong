package hust.soict.ictglobal.lab02;
import javax.swing.JOptionPane;
public class ChoosingOption{
    public static void main(String[] args){
        int option = JOptionPane.showConfirmDialog(null, "Do you want to change the first class ticket?");

        JOptionPane.showMessageDialog(null, "You've chosen: " + (option==JOptionPane.YES_OPTION?"Yes":"No"));
        System.exit(0);
    }
}
/* 
Answer:
- if users choose "Cancel", message dialog show "You've chosen: No"
- To customize the options:
    Object[] options = { "I do", "I don't" };
    JOptionPane.showOptionDialog(null, "Do you want to change the first class ticket?", "Choose", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
*/