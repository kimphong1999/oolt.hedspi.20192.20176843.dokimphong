package hust.soict.ictglobal.lab02;
import java.util.Scanner;
public class DisplayNumberDayofMonth{
    public static void main(String[] args){
        Scanner Input = new Scanner(System.in);
        int iDay = 0, iMonth, iYear;
        String strMonth = "";

        System.out.print("Please input year: ");
        iYear = Input.nextInt();
        while (iYear < 1){
            System.out.print("Please input year > 0, try again: ");
            iYear = Input.nextInt();
        }

        System.out.print("Please input month: ");
        iMonth = Input.nextInt();
        while (iMonth < 1 || iMonth > 12)
        {
            System.out.print("Please input month(from 1 to 12), try again: ");
            iMonth = Input.nextInt();
        }

        switch(iMonth){
            case 1: 
            {
                iDay = 31;
                strMonth = "January";
            }
            break;
            case 2:
            {
                if (iYear%4 == 0)
                {
                    iDay = 29;
                }
                else 
                {
                    iDay = 28;
                }
                strMonth = "February";
            }
            break;
            case 3: 
            {
                iDay = 31;
                strMonth = "March";
            }
            break;
            case 4: 
            {
                iDay = 30;
                strMonth = "April";
            }
            break;
            case 5: 
            {
                iDay = 31;
                strMonth = "May";
            }
            break;
            case 6: 
            {
                iDay = 30;
                strMonth = "June";
            }
            break;
            case 7: 
            {
                iDay = 31;
                strMonth = "July";
            }
            break;
            case 8: 
            {
                iDay = 31;
                strMonth = "August";
            }
            break;
            case 9: 
            {
                iDay = 30;
                strMonth = "September";
            }
            break;
            case 10: 
            {
                iDay = 31;
                strMonth = "October";
            }
            break;
            case 11: 
            {
                iDay = 30;
                strMonth = "November";
            }
            break;
            case 12: 
            {
                iDay = 31;
                strMonth = "December";
            }
            break;
        }
        System.out.println("Number of days of " + strMonth +", " + iYear + ": " + iDay);
    }
}