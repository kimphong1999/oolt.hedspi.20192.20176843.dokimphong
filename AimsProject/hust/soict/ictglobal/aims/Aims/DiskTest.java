package hust.soict.ictglobal.aims.Aims;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order.Order;

public class DiskTest {

	public static void main(String[] args) {
		Order anOrder = new Order();

	    DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87);
	    dvd1.setCost(19.95f);
	    dvd1.setId(1);

	    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124);
	    dvd2.setCost(24.95f);
	    dvd2.setId(2);

	    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90);
	    dvd3.setCost(18.99f);
	    dvd3.setId(3);

	    anOrder.printListOrdered();

	    System.out.println("Lucky dvd: " + anOrder.getALuckyItem().getTitle());
	    anOrder.printListOrdered();
	}

}
